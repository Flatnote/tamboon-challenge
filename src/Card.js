import React, { useEffect, useState } from 'react';
import ReactCardFlip from 'react-card-flip';
import {
  BackCard,
  BackContent,
  Button,
  CardContainer,
  LabelFlex,
} from './App.styled';
import { getStorage, ref, getDownloadURL } from 'firebase/storage';
import CardLoading from './CardLoading';

const options = [10, 20, 50, 100, 500];

const Card = ({ item, handlePay }) => {
  const [isFlipped, setIsFlipped] = useState(false);
  const [selectedAmount, setSelectedAmount] = useState(options[0]);
  const [thumbnailUrl, setThumbnailUrl] = useState();
  const [imageLoaded, setImageLoaded] = useState(false);

  const onClickDonateAndBackCard = () => setIsFlipped(!isFlipped);

  const getImageUrlFromFirebase = async () => {
    const storage = getStorage();
    const pathReference = ref(storage, `thumbnails/${item.image}`);
    const url = await getDownloadURL(pathReference);
    setThumbnailUrl(url);
  };

  useEffect(() => {
    getImageUrlFromFirebase();
  }, []);

  const onChangeValue = (event) => {
    setSelectedAmount(parseInt(event.target.value));
  };

  const payments = options.map((amount, index) => {
    return (
      <label key={`${amount}-${index}`}>
        <input
          type="radio"
          value={amount}
          name={`payment-for-${item.name}`}
          onChange={onChangeValue}
          checked={selectedAmount === amount}
        />{' '}
        {amount}
      </label>
    );
  });

  return (
    <CardContainer>
      <div className="card">
        {!imageLoaded && <CardLoading width="100%" height="100%" />}
        <ReactCardFlip isFlipped={isFlipped}>
          <div id="front">
            {thumbnailUrl && (
              <div>
                <img
                  src={thumbnailUrl}
                  alt={`thumbnail-${item.name}`}
                  onLoad={() => setImageLoaded(true)}
                />
                <div className="description">
                  <LabelFlex>
                    <p>{item.name}</p>
                    <Button onClick={onClickDonateAndBackCard}>Donate</Button>
                  </LabelFlex>
                </div>
              </div>
            )}
          </div>
          <BackCard id="back" onClick={onClickDonateAndBackCard}>
            <BackContent onClick={(e) => e.stopPropagation()}>
              <div>
                <p>Select the amount to donate (THB)</p>
              </div>
              <div>{payments}</div>
              <Button
                onClick={() => {
                  handlePay(item.id, selectedAmount, item.currency);
                }}
                margin="8px auto"
              >
                Pay
              </Button>
            </BackContent>
          </BackCard>
        </ReactCardFlip>
      </div>
    </CardContainer>
  );
};

export default Card;
