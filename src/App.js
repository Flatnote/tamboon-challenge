import axios from 'axios';
import { collection, doc, getDocs, setDoc } from 'firebase/firestore';
import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { AppContainer, GridContainer, SubTitle, Title } from './App.styled';
import Card from './Card';
import { summaryDonations } from './helpers';
import { db } from './services/firebase';
import { container, item } from './utils/motion-variants';

import './App.css';

const App = ({ dispatch, donate }) => {
  const [charities, setCharities] = useState([]);
  const [paymentSnapshot, setPaymentSnapshot] = useState([]);

  const getCharitiesFromFirestore = async () => {
    const querySnapshot = await getDocs(collection(db, 'charities'));
    querySnapshot.forEach((doc) => {
      setCharities((prev) => [...prev, doc.data()]);
    });
  };

  const getPaymentsFromFirebase = async () => {
    setPaymentSnapshot(await getDocs(collection(db, 'payments')));
  };

  const getCharities = async () => {
    const { data } = await axios('http://localhost:3001/charities');
    setCharities(data);
  };

  const getPayments = async () => {
    const { data } = await axios('http://localhost:3001/payments');
    dispatch({
      type: 'UPDATE_TOTAL_DONATE',
      amount: summaryDonations(data.map((item) => item.amount)),
    });
  };

  useEffect(() => {
    if (process.env.NODE_ENV === 'development') {
      Promise.all([getCharities(), getPayments()]);
    }
    if (process.env.NODE_ENV === 'production') {
      Promise.all([getCharitiesFromFirestore(), getPaymentsFromFirebase()]);
    }

    return () => {};
  }, []);

  useEffect(() => {
    if (paymentSnapshot) {
      paymentSnapshot.forEach((doc) => {
        const payment = doc.data();
        if (payment.amount) {
          dispatch({
            type: 'UPDATE_TOTAL_DONATE',
            amount: payment.amount,
          });
        }
      });
    }
    return () => {};
  }, [paymentSnapshot]);

  const handlePay = async (id, amount, currency) => {
    if (process.env.NODE_ENV === 'development') {
      await axios.post('http://localhost:3001/payments', {
        charitiesId: id,
        amount,
        currency,
      });
      dispatch({
        type: 'UPDATE_TOTAL_DONATE',
        amount,
      });
      swal('', `Thanks for donate ${amount}`, 'success');
      setTimeout(() => {
        dispatch({
          type: 'UPDATE_MESSAGE',
          message: '',
        });
      }, 2000);
    }
    if (process.env.NODE_ENV === 'production') {
      await setDoc(doc(db, 'payments', (paymentSnapshot.size + 1).toString()), {
        charitiesId: id,
        amount,
        currency,
      }).catch((error) => {
        console.log(error);
      });

      dispatch({
        type: 'UPDATE_TOTAL_DONATE',
        amount,
      });

      swal('', `Thanks for donate ${amount} ${currency}`, 'success');

      setTimeout(() => {
        dispatch({
          type: 'UPDATE_MESSAGE',
          message: '',
        });
      }, 2000);
    }
  };

  const renderCards = charities.map((item, i) => (
    <Card key={i} item={item} handlePay={handlePay} />
  ));

  return (
    <AppContainer variants={container} initial="hidden" animate="visible">
      <motion.div variants={item}>
        <Title>Tamboon React Challenge</Title>
        <SubTitle>All donations: {donate}</SubTitle>
        <GridContainer>{renderCards}</GridContainer>
      </motion.div>
    </AppContainer>
  );
};

export default connect((state) => state)(App);
