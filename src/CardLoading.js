import React from 'react';
import ContentLoader from 'react-content-loader';

const CardLoading = (props) => (
  <ContentLoader
    speed={2}
    width={560}
    height={346}
    viewBox="0 0 560 346"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <path d="M 0 0 h 560 v 290 H 0 z M 22 312 h 217 v 34 H 22 z M 449 313 h 76 v 33 h -76 z" />
  </ContentLoader>
);

export default CardLoading;
