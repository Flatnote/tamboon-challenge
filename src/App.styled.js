import styled from 'styled-components';
import { device } from './device';
import { motion } from 'framer-motion';

export const AppContainer = styled(motion.div)``;

export const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 16px;
  grid-auto-rows: minmax(100px, auto);

  @media ${device.tablet} {
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${device.laptop} {
    grid-template-columns: repeat(3, 1fr);
  }
`;

export const Title = styled.h1`
  text-align: center;
`;

export const SubTitle = styled.h2`
  text-align: center;
`;

export const MessageText = styled.p`
  color: red;
  margin: 1em 0;
  font-weight: bold;
  font-size: 16px;
  text-align: center;
`;

export const CardContainer = styled.div`
  margin: 2rem;
  display: flex;
  justify-content: center;
  align-items: center;

  .description {
    padding: 8px 16px;
  }

  .card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
    border-radius: 5px;
    width: 100%;
  }

  .card:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
  }

  img {
    border-radius: 5px 5px 0 0;
    width: 100%;
    max-height: 290px;
    object-fit: cover;
  }
`;

export const Button = styled.button`
  background-color: white;
  border: 2px solid #3c76f7;
  color: #3c76f7;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  height: 36px;
  margin: ${({ margin }) => (margin ? margin : '4px 2px')};
  cursor: pointer;
`;

export const LabelFlex = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const BackCard = styled.div`
  min-height: 357px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const BackContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: center;
  /* z-index: 1; */
`;
