import { doc, setDoc } from '@firebase/firestore';
import axios from 'axios';

export const syncPaymentsWithFirestore = async () => {
  const { data } = await axios('http://localhost:3001/payments');
  await Promise.all(
    data.map(async (payment) => {
      await setDoc(doc(db, 'payments', payment.id.toString()), payment);
    })
  );
  swal('Success', 'Payments synced successfully', 'success');
};
